//Written by Shane Roberts
public class Inventory {
	String name;
	//ORDER: Item 102, 215, 410, 525, 711.
	
	final String[] itemNames = {"Item 102", "Item 215", "Item 410", "Item 525", "Item 711"};
	int items[] = new int[5];
	
	public Inventory(String n) {
		name = n;
	}

	public String getName() {
		return name;
	}
	
	public void initInventory(String inv) {
		String[] splitInv = inv.split(",\\s+");
		for (int x = 0; x < splitInv.length; x++) {
			items[x] = Integer.parseInt(splitInv[x]);
		}
	}
	
	public String getInventoryString() {
		String inventoryString = name + " Warehouse Inventory: ";
		for (int x = 0; x < items.length; x++) {
			inventoryString += itemNames[x] + ": " + items[x];
			if (x + 1 < items.length) inventoryString += ", ";
			else inventoryString += ".";
		}
		return inventoryString;
	}
	
	public String getRawInventoryString() {
		String inventoryString = "";
		for (int x = 0; x < items.length; x++) {
			inventoryString += items[x];
			if (x + 1 < items.length) inventoryString += ", ";
			else inventoryString += "\n";
		}
		return inventoryString;
	}
	
	public int getQuantityOf(String itemName) {
		if (itemName.equals("102")) {
			return items[0];
		} else if (itemName.equals("215")) {
			return items[1];
		} else if (itemName.equals("410")) {
			return items[2];
		} else if (itemName.equals("525")) {
			return items[3];
		} else {
			return items[4];
		}
	}
	
	private int getItemIndexOf(String itemName) {
		if (itemName.equals("102")) {
			return 0;
		} else if (itemName.equals("215")) {
			return 1;
		} else if (itemName.equals("410")) {
			return 2;
		} else if (itemName.equals("525")) {
			return 3;
		} else {
			return 4;
		}
	}
	
	public void addStock(String item, int amount) {
		items[getItemIndexOf(item)] += amount;
	}
	
	public void subtractStock(String item, int amount) {
		items[getItemIndexOf(item)] -= amount;
	}
}
