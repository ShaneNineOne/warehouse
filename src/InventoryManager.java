import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileWriter;

public class InventoryManager {

	Inventory inventories[] = new Inventory[6];

	public InventoryManager() {
		inventories[0] = new Inventory("Atlanta");
		inventories[1] = new Inventory("Baltimore");
		inventories[2] = new Inventory("Chicago");
		inventories[3] = new Inventory("Denver");
		inventories[4] = new Inventory("Ely");
		inventories[5] = new Inventory("Fargo");
	}
	
	public void initializeInventories() {
		//Initialize inventories.
		try{
			FileInputStream fstream = new FileInputStream("inventory.txt");
	  
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
	  
			for (int x = 0; x < inventories.length; x++) {
				inventories[x].initInventory(br.readLine());
			}

			in.close();  
		} catch (Exception e) {//Catch exception if any
		   System.err.println("Error: " + e.getMessage());
		}
	}
	
	public void updateInventories() {
		try{
			FileWriter fw = new FileWriter("inventory.txt");
			 
			for (int i = 0; i < inventories.length; i++) {
				fw.write(inventories[i].getRawInventoryString());
			}
		 
			fw.close(); 
		} catch (Exception e) {//Catch exception if any
		   System.err.println("Error: " + e.getMessage());
		}
	}
	
	public void displayInventories() {
		for (int x = 0; x < inventories.length; x++) {
			System.out.println(inventories[x].getInventoryString());
		}
	}
	
	public void runInventoryTransactions() {	
		try{
			FileInputStream fstream = new FileInputStream("transactions.txt");
			  
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));	
			
			String command;
			
			while ((command = br.readLine()) != null) {
				String[] splitCommand = command.split(",\\s+");
				if (splitCommand[0].equalsIgnoreCase("p")) {
					purchase(splitCommand);
				} else {
					sell(splitCommand);
				}
			}
			
		} catch (Exception e) {//Catch exception if any
			   System.err.println("Error: " + e.getMessage());
		}
	}
	
	private void purchase(String command[]) {
		int lowestQuantityIndex = 0;
		int lowestQuantity = Integer.MAX_VALUE;//Start with the highest possible integer value... we're bound to find things lower than it.
		
		for (int x = 0; x < inventories.length; x++) {
			if (inventories[x].getQuantityOf(command[1]) < lowestQuantity) {
				lowestQuantityIndex = x;
				lowestQuantity = inventories[x].getQuantityOf(command[1]);
			}
		}
		System.out.println("Purchasing " + command[2] + " of item " + command[1] + ". Placing it into the " + inventories[lowestQuantityIndex].getName() + " warehouse.");
		inventories[lowestQuantityIndex].addStock(command[1], Integer.parseInt(command[2]));
	}
	
	private void sell(String command[]) {
		int highestQuantityIndex = 0;
		int highestQuantity = 0;
		
		for (int x = 0; x < inventories.length; x++) {
			if (inventories[x].getQuantityOf(command[1]) > highestQuantity) {
				highestQuantityIndex = x;
				highestQuantity = inventories[x].getQuantityOf(command[1]);
			}
		}
		System.out.println("Selling " + command[2] + " of item " + command[1] + " from the " + inventories[highestQuantityIndex].getName() + " warehouse.");
		inventories[highestQuantityIndex].subtractStock(command[1], Integer.parseInt(command[2]));
	}
}
