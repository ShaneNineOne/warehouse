import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;

//Written by Shane Roberts

public class Warehouses {
	
	Inventory inventories[] = new Inventory[6];

	public static void main(String[] args) {
			
		InventoryManager inventoryManagement = new InventoryManager();
		inventoryManagement.initializeInventories();
		System.out.println("\nPre transaction inventory values:\n");
		inventoryManagement.displayInventories();
		System.out.println("\nExecute transactions: \n");
		inventoryManagement.runInventoryTransactions();
		System.out.println("\nPost transaction inventory values:\n");
		inventoryManagement.displayInventories();
		inventoryManagement.updateInventories();
	
	}
}
	

